#!/bin/bash

THIS_FILE='/home/minecraft/minecraft-tools'
BACKUPPATH='/home/apache/www/html/backup'
WORLD='world'
MC_PATH='/home/minecraft/server'
VERSION='1.0.1'
SRV_PATH=$MC_PATH'/'$VERSION
SRV_BIN='minecraft_server.jar'
SRV_CONF='server.properties'
INVOCATION="java -Xmx950M -Xms950M -XX:+UseConcMarkSweepGC -XX:+CMSIncrementalPacing -XX:ParallelGCThreads=2 -XX:+AggressiveOpts -jar "$SRV_BIN
INVOCATION_LOW="java -Xms32M -Xmx386M -jar "$SRV_BIN" nogui"
INSERTDATE='['`date +%Y.%m.%d`'] @ '`date +%H:%M:%S`' : '
DATE=`date +%Y-%m-%d`
LOG_RAW='server.log'
LOG_MC_TOOLS='minecraft-tools.log'
PATH_LOG_RAW=$SRV_PATH'/'$LOG_RAW
PATH_LOG_NEAT='/home/minecraft/logs'
MC_SERVER_URL=http://s3.amazonaws.com/MinecraftDownload/launcher/minecraft_server.jar?v=`date +%Y-%m-%d | sed "s/[^a-zA-Z0-9]/_/g"`

function mc_srv_start() {
	if ps ax | grep -v grep | grep -v -i SCREEN | grep $SRV_BIN > /dev/null
	then
		echo $INSERTDATE "Minecraft is already running!"
	else
		echo $INSERTDATE "Starting Minecraft..."
		echo $INSERTDATE "START : Minecraft est en train de démarrer ..." >> $PATH_LOG_NEAT/$LOG_MC_TOOLS
		cd $SRV_PATH && screen -dmS minecraft_server $INVOCATION
		sleep 7
		if ps ax | grep -v grep | grep -v -i SCREEN | grep $SRV_BIN > /dev/null
		then
			echo $INSERTDATE "Minecraft is now running."
			echo $INSERTDATE "START : Minecraft a démarré." >> $PATH_LOG_NEAT/$LOG_MC_TOOLS
			echo "Le serveur #Minecraft a démarré correctement." | bti
		else
			echo $INSERTDATE "Error! Could not start Minecraft!"
			echo $INSERTDATE "START : Minecraft n'a pas pu démarrer." >> $PATH_LOG_NEAT/$LOG_MC_TOOLS
			echo "Une erreur est survenue durant le démarrage du serveur #Minecraft :(" | bti
		fi
	fi
}


function mc_mini_start() {
	if ps ax | grep -v grep | grep -v -i SCREEN | grep $SRV_BIN > /dev/null
	then
		echo $INSERTDATE "Minecraft is already running!"
	else
		echo $INSERTDATE "Starting Minecraft..."
		echo $INSERTDATE "START : le serveur Minecraft est en train de démarrer." >> $PATH_LOG_NEAT/$LOG_MC_TOOLS
		cd $SRV_PATH && screen -dmS minecraft_server $INVOCATION_LOW
		sleep 7
		if ps ax | grep -v grep | grep -v -i SCREEN | grep $SRV_BIN > /dev/null
		then
			echo $INSERTDATE "Minecraft is now running."
			echo $INSERTDATE "START : le serveur Minecraft a démarré." >> $PATH_LOG_NEAT/$LOG_MC_TOOLS
			echo "Le serveur #Minecraft a démarré correctement." | bti
		else
			echo $INSERTDATE "Error! Could not start Minecraft!"
			echo $INSERTDATE "START : le serveur Minecraft n'a pas pu démarrer." >> $PATH_LOG_NEAT/$LOG_MC_TOOLS
			echo "Une erreur est survenue durant le démarrage du serveur #Minecraft :(" | bti
		fi
	fi
}


function mc_srv_stop() {
	if ps ax | grep -v grep | grep -v -i SCREEN | grep $SRV_BIN > /dev/null
	then
		echo $INSERTDATE "Stopping Minecraft"
		echo $INSERTDATE "STOP : le serveur Minecraft est en train de s'éteindre." >> $PATH_LOG_NEAT/$LOG_MC_TOOLS
		screen -p 0 -S minecraft -X eval 'stuff "say LE SERVEUR VA ÊTRE ÉTEINT DANS 10 SECONDES !!!"\015'
		screen -p 0 -S minecraft -X eval 'stuff "save-all"\015'
		sleep 10
		screen -p 0 -S minecraft -X eval 'stuff "stop"\015'
		sleep 7
	else
		echo $INSERTDATE "Minecraft was not running."
		echo $INSERTDATE "STOP : le serveur Minecraft n'est pas actif." >> $PATH_LOG_NEAT/$LOG_MC_TOOLS
	fi
	if ps ax | grep -v grep | grep -v -i SCREEN | grep $SRV_BIN > /dev/null
	then
		echo $INSERTDATE "Error! Minecraft could not be stopped."
	else
		echo $INSERTDATE "Minecraft is stopped."
		echo $INSERTDATE "STOP : le serveur Minecraft s'est arrêté." >> $PATH_LOG_NEAT/$LOG_MC_TOOLS
		echo "Le serveur #Minecraft s'est arrêté correctement." | bti
	fi
}


function mc_srv_backup() {
	# SAVE-OFF
	if ps ax | grep -v grep | grep -v -i SCREEN | grep $SRV_BIN > /dev/null
	then
		echo $INSERTDATE "Minecraft is running... suspending saves"
		echo $INSERTDATE "BACKUP : sauvegarde du serveur Minecraft ..." >> $PATH_LOG_NEAT/$LOG_MC_TOOLS
		screen -p 0 -S minecraft -X eval 'stuff "say SAUVEGARDE DU SERVEUR EN COURS ..."\015'
		screen -p 0 -S minecraft -X eval 'stuff "save-off"\015'
		screen -p 0 -S minecraft -X eval 'stuff "save-all"\015'
		sync
		sleep 10
	else
		echo $INSERTDATE "Minecraft is not running. Not suspending saves."
		echo $INSERTDATE "BACKUP : sauvegarde impossible, serveur inactif." >> $PATH_LOG_NEAT/$LOG_MC_TOOLS
	fi

	# BACKUP WORLD
	echo $INSERTDATE "Backing up minecraft world..."
	echo $INSERTDATE "BACKUP : sauvegarde du monde Minecraft ..." >> $PATH_LOG_NEAT/$LOG_MC_TOOLS
	if [ -f $BACKUPPATH/mc_world_`date +%Y.%m.%d_%H:%M`.tar.gz ]
	then
		echo $INSERTDATE "World is already backed up"
		echo $INSERTDATE "BACKUP : le monde a déjà été sauvegardé." >> $PATH_LOG_NEAT/$LOG_MC_TOOLS
		continue
	else
		tar czf $BACKUPPATH/mc_world_`date +%Y.%m.%d_%H:%M`.tar.gz $SRV_PATH/
		echo $INSERTDATE "World is now backed up"
		echo $INSERTDATE "BACKUP : le monde est sauvegardé." >> $PATH_LOG_NEAT/$LOG_MC_TOOLS
		break
	fi

	# BACKUP SERVER
	echo $INSERTDATE "Backing up minecraft_server.jar"
	echo $INSERTDATE "BACKUP : sauvegarde de l'exécutable Minecraft ..." >> $PATH_LOG_NEAT/$LOG_MC_TOOLS
	if [ -f $BACKUPPATH/mc_srv_`date +%Y.%m.%d_%H.%M`.tar.gz ]
	then
		echo $INSERTDATE "Server is already backed up"
		echo $INSERTDATE "BACKUP : l'exécutable est déjà sauvegardé." >> $PATH_LOG_NEAT/$LOG_MC_TOOLS
		continue
	else
		tar czf $BACKUPPATH/mc_srv_`date +%Y.%m.%d_%H:%M`.tar.gz $SRV_PATH/$SRV_BIN
		echo $INSERTDATE "Server backup is now complete"
		echo $INSERTDATE "BACKUP : l'exécutable est sauvegardé.." >> $PATH_LOG_NEAT/$LOG_MC_TOOLS
	fi

	# SAVE-ON
	if ps ax | grep -v grep | grep -v -i SCREEN | grep $SRV_BIN > /dev/null
	then
		echo $INSERTDATE "Minecraft is running... re-enabling saves"
		echo $INSERTDATE "BACKUP : le serveur Minecraft est actif, réactivation des sauvegardes." >> $PATH_LOG_NEAT/$LOG_MC_TOOLS
		screen -p 0 -S minecraft -X eval 'stuff "save-on"\015'
		screen -p 0 -S minecraft -X eval 'stuff "say SAUVEGARDE DU SERVEUR TERMINÉE !"\015'
		echo "Le serveur #Minecraft a été correctement sauvegardé." | bti
		echo $INSERTDATE "BACKUP : le serveur Minecraft a été correctement sauvegardé." >> $PATH_LOG_NEAT/$LOG_MC_TOOLS
	else
		echo $INSERTDATE "Minecraft is not running. Not resuming saves."
		echo $INSERTDATE "BACKUP : le serveur Minecraft est inactif, la sauvegarde ne reprend pas." >> $PATH_LOG_NEAT/$LOG_MC_TOOLS
	fi
}


function mc_log_parser() {
	#
	# Rotation et parsing de log pour en extraire les info de connexion et les discussions
	#

	#
	# Variables
	STR_LOGGING='lost connection\|logged in with'
	STR_CHAT='<*> '
	YEAR=`date +%Y`
	MONTH=`date +%m`
	DAY=`date +%d`
	TREE[0]=$YEAR
	TREE[1]=$MONTH
	STR_ROTATEFILE=$DAY'-raw.log'
	STR_CHATLOG=$DAY'-chat.log'
	STR_CONNECTIONLOG=$DAY'-connection.log'
	STR_COMMANDLOG=$DAY'-command.log'

	#
	# ROTATION
	#

	# FOR-loop to test if directory doesn't exist
	cd $PATH_LOG_NEAT
	for directory in "${!TREE[@]}"; do
		if [[ ! -d ${TREE[$directory]} ]]; then
			mkdir ${TREE[$directory]}
		fi
		cd ${TREE[$directory]}
	done

	# Deeply in the tree, moving the logfile
	mv $PATH_LOG_RAW $STR_ROTATEFILE



	#
	# PARSING
	#

	# Who played on the server today ?
	grep -E 'lost connection|logged in with|Disconnecting' $STR_ROTATEFILE > $STR_CONNECTIONLOG

	# What did the players talked about ?
	grep -E '<(felix|alessio|antoine|flo|bastien|denis|vincent|seb|ugo|elisa|lucas|mathieu|benoit|gui)> ' $STR_ROTATEFILE > $STR_CHATLOG
	
	# Which commands players tried ?
	grep -E 'issued server command' $STR_ROTATEFILE > $STR_COMMANDLOG
}


function mc_conf_regenerator() {
	#
	# Modifier la date d'édition (commentaire)
	#

	# récupérer le contenu de la ligne 2
	BUFFER=`sed -n '2p' $SRV_PATH/$SRV_CONF`
	# remplacement 
	sed -i "2 s/`echo $BUFFER`/\#Last edit : `echo $DATE`/" $SRV_PATH/$SRV_CONF
	echo $INSERTDATE "CONF REGEN : modification de la date de modification." >> $PATH_LOG_NEAT/$LOG_MC_TOOLS


	#
	# Modifier le level-seed
	#

	# générer un level-seed de 30 caractères alpha-numériques
	NEW_LVL_SEED=`tr -cd '[:alnum:]' < /dev/urandom | fold -w30 | head -n1`
	# retrouver quel numéro de ligne contient cette variable
	LN_NUM_LVL_SEED=`cat $SRV_PATH/$SRV_CONF | grep -n level-seed | cut -d: -f1`
	# récupérer le contenu de l'ancien level-seed
	OLD_LVL_SEED=`cat $SRV_PATH/$SRV_CONF | grep level-seed | cut -d\= -f2`
	sed -i "`echo $LN_NUM_LVL_SEED` s/`echo $OLD_LVL_SEED`/`echo $NEW_LVL_SEED`/" $SRV_PATH/$SRV_CONF
	echo $INSERTDATE "CONF REGEN : modification du level-seed." >> $PATH_LOG_NEAT/$LOG_MC_TOOLS
	echo "Un nouveau monde #Minecraft a été généré ... #surprise" | bti


	#
	# Modifier le nom du monde
	#

	# retrouver quel numéro de ligne contient cette variable
	LN_NUM_LVL_NAME=`cat $SRV_PATH/$SRV_CONF | grep -n level-name | cut -d: -f1`
	# récupérer le contenu de l'ancien level-name
	OLD_LVL_NAME=`cat $SRV_PATH/$SRV_CONF | grep level-name | cut -d\= -f2`
	sed -i "`echo $LN_NUM_LVL_NAME` s/`echo $OLD_LVL_NAME`/`echo $WORLD`-`echo $DATE`/" $SRV_PATH/$SRV_CONF
	echo $INSERTDATE "CONF REGEN : modification du nom du monde." >> $PATH_LOG_NEAT/$LOG_MC_TOOLS
}


function mc_srv_update() {
	echo "Le serveur est actuellement en version :" $VERSION
	# Quelle version est la plus récente ?
	latest_version=`wget -q -O index.html http://www.minecraftwiki.net/wiki/Minecraft_Wiki && grep -E 'Current PC version' index.html | cut -d:  -f3 | sed s/'<b>'/':'/ | sed s/'<\/b>'/':'/ | cut -d: -f2`
	# suppression du fichier temporaire
	rm index.html
	# Est-elle plus récente que la version actuelle du serveur ?
	if [ "$latest_version" != "$VERSION" ]
	then
		echo "Une nouvelle version est disponible :" $latest_version
		# création du répertoire
		mkdir $MC_PATH/$latest_version && cd $MC_PATH/$latest_version
		# téléchargement de l'exécutable
		echo $INSERTDATE "UPDATE : téléchargement de l'exécutable le plus récent ..." >> $PATH_LOG_NEAT/$LOG_MC_TOOLS
		wget -q -O $MC_PATH/$latest_version/$SRV_BIN $MC_SERVER_URL
		
		# récupération des anciens paramètres
		echo $INSERTDATE "UPDATE : copie de la config et des joueurs ..." >> $PATH_LOG_NEAT/$LOG_MC_TOOLS
		cp $SRV_PATH/{admins,banned-ips,banned-players,ops,white-list}.txt $MC_PATH/$latest_version/
		cp $SRV_PATH/server.properties $MC_PATH/$latest_version/
		
		# modification du script
		echo $INSERTDATE "UPDATE : modification du script ..." >> $PATH_LOG_NEAT/$LOG_MC_TOOLS
		sed -i "s/VERSION='`echo $VERSION`'/VERSION='`echo $latest_version`'/" $THIS_FILE
		
		# confirmation des opérations
		echo "Minecraft successfully updated."
		echo "Le serveur Minecraft est passé en "$latest_version" !" | bti
		echo $INSERTDATE "UPDATE : mise-à-jour de l'exécutable réussie !" >> $PATH_LOG_NEAT/$LOG_MC_TOOLS
	else
		echo $INSERTDATE "UPDATE : pas de nouvelle version, patience ..." >> $PATH_LOG_NEAT/$LOG_MC_TOOLS
		echo "Le serveur exécute la dernière version de Minecraft."
	fi
}


mc_srv_command() {
  command="$1";
  if ps ax | grep -v grep | grep -v -i SCREEN | grep $SRV_BIN > /dev/null
  then
    pre_log_len=`wc -l "$PATH_LOG_RAW" | awk '{print $1}'`
    echo "$SRV_BIN is running... executing command"
    screen -p 0 -S minecraft -X eval 'stuff \"$command\"\015'
    sleep .1 # assumes that the command will run and print to the log file in less than .1 seconds
    # print output
    tail -n $[`wc -l "$PATH_LOG_RAW" | awk '{print $1}'`-$pre_log_len] "$PATH_LOG_RAW"
  fi
}


mc_post() {
  command="$1";
  echo $command | bti
  echo $INSERTDATE "POST : message publié → "$command >> $PATH_LOG_NEAT/$LOG_MC_TOOLS
}


#Start-Stop here
case "$1" in
	start)
		mc_srv_start
		;;
	stop)
		mc_srv_stop
		;;
	restart)
		mc_srv_stop
		mc_srv_start
		;;
	update)
		mc_srv_update
		;;
	backup)
		mc_srv_backup
		;;
	parse-log)
		mc_log_parser
		echo $INSERTDATE "LOGPARSER : les logs bruts sont triés." >> $PATH_LOG_NEAT/$LOG_MC_TOOLS
		;;
	conf-regen)
		mc_conf_regenerator
		;;
	status)
		if ps ax | grep -v grep | grep -v -i SCREEN | grep $SRV_BIN > /dev/null
		then
			echo "$SRV_BIN is running."
		else
			echo "$SRV_BIN is not running."
		fi
		;;
	command)
		if [ $# -gt 1 ]
		then
			shift
			mc_srv_command "$*"
		else
			echo "Must specify server command (try 'help'?)"
		fi
		;;
	post)
		if [ $# -gt 1 ]
		then
			shift
			mc_post "$*"
		else
			echo "Veuillez spécifier un message à publier sur SN (try 'help'?)"
		fi
		;;
	*)
		echo "Usage: /etc/init.d/minecraft {start|stop|update|backup|status|restart|parse-log|conf-regen|command \"server command\"|post \"message\"}"
		exit 1
		;;
esac

exit 0
